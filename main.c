
#include "includes/cub3d.h"

int ft_check_arg(int ac, char **argv)
{
    int fd;

    if (ac == 1 || ac > 3)
    {
        write(1, "Invalid number of arguments\n", 27);
        return (-1);
    }
    else if (ac == 3 && ((ft_strncmp(argv[2], "--save", 6))))
    {
        write(1, "Invalid call\n", 12);
        return (-1);
    }
    else if ((fd = open(argv[1], O_RDONLY)) <= 0)
    {
        write(1, "No such file\n", 12);
        return (-1);
    }
    return (1);
}

int init_game(t_data *mlx, char *argv)
{
    init_params(mlx);
    if ((parse(argv, mlx)) < 0)
        return(-1);
    if ((map_size(mlx)) < 0)
    {
        write(1, "Malloc problem\n", 14);
        return (-1);
    }
    if (!(mlx->mlx = mlx_init()))
        return(-1);
    finish_init(mlx);
    mlx->win = mlx_new_window(mlx->mlx, mlx->res.w, mlx->res.h, "Cub 3D");
    mlx->img = mlx_new_image(mlx->mlx, mlx->res.w, mlx->res.h);
    mlx->addr = (unsigned int *)mlx_get_data_addr(mlx->img, &mlx->bits_per_pixel, &mlx->line_length,
                                 &mlx->endian);
    ray_casting(mlx);
    free(mlx->img);
    return (1);
}

int exit_all(t_data *mlx)
{
    free(mlx->img);
    exit(0);
    return (-1);
}

int main(int ac, char **argv)
{
    t_data  mlx;

    if (ft_check_arg(ac, argv) < 0)
        return(-1);
    else if (ac == 2)
    {
        if ((init_game(&mlx, argv[1])) < 0)
            return(exit_all(&mlx));
        mlx_hook(mlx.win, 2, 1L<<0, key_press, &mlx);
        // mlx_key_hook(mlx.win, key_release, &mlx);
        // mlx_hook(mlx.win, 17, 1L<<0, exit_all, &mlx);
        // mlx_loop_hook(mlx.mlx, run_game, &mlx);
        mlx_loop(mlx.mlx);
    }
    return (0);
}
