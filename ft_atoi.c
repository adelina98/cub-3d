#include "includes/cub3d.h"

int	ft_atoi(const char *str)
{
	long	result;
	int		sign;
	int		i;

	result = 0;
	sign = 1;
	i = 0;
	while ((*str >= 9 && *str <= 13) || *str == 32)
		str++;
	if (*str == '-')
	{
		sign = -1;
		str++;
	}
	else if (*str == '+')
		str++;
	while (str[i] >= 48 && str[i] <= 57)
	{
		if ((result * 10) < result)
			return ((sign == 1) ? -1 : 0);
		result = result * 10 + str[i] - '0';
		i++;
	}
	return ((int)result * sign);
}
