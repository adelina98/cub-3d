
#ifndef CUB3D_H
# define CUB3D_H

#include "minilibx/mlx.h"
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <fcntl.h>

# define BUFFER_SIZE 1024
# define WIN_W 1024
# define WIN_H 512

typedef struct s_res
{
    int         w;
    int         h;
}              t_res;

typedef struct  s_error
{
    int         lin;
    int         m;
    int         p;
}               t_error;

typedef struct  s_ray
{
    float   x;
    float   y;
    float  d_d_x;
    float  d_d_y;
}               t_ray;

typedef struct  s_step
{
    float   x;
    float   y;
    float s_d_X;
    float s_d_Y;
}               t_step;

typedef struct s_map
{
    int s;
    int *s_ord;
    int y;
    int x;
    int mapX;
    int mapY;
    char    **map;
}              t_map;

typedef struct  s_direction
{
    float   x;
    float   y;
    float   oldx;
    float   oldy;
}               t_direction;

typedef struct  s_position
{
    float   x;
    float   y;
    char    c;
}               t_position;

typedef struct s_texture
{
    unsigned int	*n;
	unsigned int	*s;
	unsigned int	*e;
	unsigned int	*w;
	unsigned int	*i;
	unsigned int	c;
	unsigned int	f;
    int             er;
    unsigned int    *color;
}              t_texture;

typedef struct  s_plane
{
    float   x;
    float   y;
    float   oldx;
    float   oldy;
}               t_plane;

typedef struct		s_spr
{
	int		numsprites;
	void	*spr_tex;
	int		*color;
	int		sprwidth;
	int		sprheight;
	float	spritex;
	float	spritey;
	float	invdet;
	float	transformx;
	float	transformy;
	int		spritescreenx;
	int		drawstarty;
	int		drawendy;
	int		drawstartx;
	int		drawendx;
	int		stripe;
	int		texx;
	int		texy;
	float	*zbuffer;
	float	*sprites_x;
	float	*sprites_y;
	float	spritedistance;
	int		vmovescreen;
}					t_spr;

typedef struct  s_data
{
    void        *mlx;
    void        *win;
    void        *img;
    unsigned int  *addr;
    int         bits_per_pixel;
    int         line_length;
    int         endian;
    t_res       res;
    float       cameraX;
    int         d_start;
    int         d_end;
    t_map       map;
    t_ray       ray;
    t_direction dir;
    t_step      step;
    t_texture   tex;
    float      wall_dist;
    int         l_height;
    int         wall_h;
    int         x;
    int         wallx;
    int         tex_x;
    int         tex_y;
    int         tex_w;
    int         tex_h;
    float       tex_pos;
    int         wall_side;
    float      move_speed;
    float      rot_speed;
    float       st;
    t_position  pos;
    t_error     er;
    t_spr       spr;
    t_plane     plane;
}               t_data;


void    init_params(t_data *mlx);
int     finish_init(t_data *mlx);
int     parse(char *file, t_data *mlx);
int    get_position(t_data *mlx);
int     ft_recheck_map(t_data *mlx);
int     map_size(t_data *mlx);
int     ft_get_map(t_data *mlx, char *line, int *i);
int ft_resolution(t_data *mlx, char *line);
int  ft_floor_ceilling(unsigned int *color, char *line);
int  ft_texture(t_data *mlx, unsigned int **adr, char *line);
void ray_casting(t_data *mlx);
void    ft_draw_line(t_data *mlx, int i);
void    ft_line_height(t_data *mlx);
void    ft_cast_textures(t_data *mlx);
void    choose_tex(t_data *mlx);
int key_press(int keycode, t_data *mlx);
int	ft_strncmp(const char *s1, const char *s2, size_t n);
int	ft_atoi(const char *str);
int	ft_isalpha(int c);
int	ft_isdigit(int c);
char		**ft_split(char *s, char c);
size_t	ft_strlcpy(char *dst, char *src, size_t size);
int exit_all(t_data *mlx);
int		ft_strlen(char *str);
void	*ft_memcpy(void *dest, const void *src, size_t n);
char	*ft_strchr(const char *s, int c);
char	*ft_strdup(char *s);
char	*ft_strjoin(char *s1, char *s2);
int		get_next_line(int fd, char **line);


#endif
